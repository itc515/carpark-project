/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bcccp.carpark.entry;

/**
 *
 * @author Kashif
 */

import bcccp.carpark.CarSensor;
import bcccp.carpark.Carpark;
import bcccp.carpark.Gate;
import bcccp.carpark.ICarSensor;
import bcccp.carpark.ICarpark;
import bcccp.carpark.IGate;
import bcccp.carpark.paystation.PaystationController;
import bcccp.tickets.adhoc.AdhocTicketDAO;
import bcccp.tickets.adhoc.AdhocTicketFactory;
import bcccp.tickets.adhoc.IAdhocTicket;
import bcccp.tickets.adhoc.IAdhocTicketDAO;
import bcccp.tickets.season.ISeasonTicket;
import bcccp.tickets.season.ISeasonTicketDAO;
import bcccp.tickets.season.IUsageRecord;
import bcccp.tickets.season.SeasonTicket;
import bcccp.tickets.season.SeasonTicketDAO;
import bcccp.tickets.season.UsageRecordFactory;
import java.util.Calendar;
import java.util.Date;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.*;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class)
public class EntryControllerIT {
    
    EntryController myEntryController;
    ICarpark carPark;
    IGate entryGate;
    ICarSensor os;
    ICarSensor is;
    IEntryUI ui;
    IAdhocTicket adhocTicket;
    ISeasonTicket seasonTicket;
    ISeasonTicketDAO seasonTicketDAO;
    IAdhocTicketDAO adhocTicketDAO;
    IUsageRecord usageRecord;
    String CarParkName = "Test Car Park";
    int CarParkCapacity = 3;
    String seasonTktCode = "S1111";
      
    
    public EntryControllerIT() {
    }
    
     @Before
    public void setUp() {
        entryGate = mock(Gate.class);
        os = mock(ICarSensor.class);
        is = mock(ICarSensor.class);
        ui = mock(IEntryUI.class);

        seasonTicketDAO = new SeasonTicketDAO(new UsageRecordFactory());
        adhocTicketDAO = new AdhocTicketDAO(new AdhocTicketFactory());
        carPark = new Carpark(CarParkName, CarParkCapacity, adhocTicketDAO, seasonTicketDAO);

        
        myEntryController = new EntryController(carPark,entryGate,is,os,ui);
    }
    
    @After
    public void tearDown() {
        myEntryController = null;
        carPark = null;
        adhocTicketDAO = null;
        seasonTicketDAO = null;
        adhocTicket = null;
    }
    
    @Test
    public void testTicketInsertedSeason() {
        System.out.println("Integration Test : ticketInserted : Season");
        
        myEntryController.ticketTaken();
        myEntryController.ticketInserted("InvalidCode");
        verify(ui, times(1)).display("Ticket Rejected"); 
                

        
        carPark.isSeasonTicketValid(seasonTktCode);
        carPark.recordSeasonTicketEntry(seasonTktCode);
        myEntryController.ticketInserted(seasonTktCode);
        verify(ui, times(0)).display("Take Ticket");
        

    }
    
    
    @Test
    public void testTicketTaken() {
        System.out.println("Integration Test : ticketTaken");        
        myEntryController.ticketTaken();
        verify(entryGate, times(0)).raise();
        
        //Make "isTicketValidated" to true
        myEntryController.ticketInserted(seasonTktCode);
        
        myEntryController.ticketTaken();
        verify(entryGate, times(0)).raise();
    }
    
    
}
