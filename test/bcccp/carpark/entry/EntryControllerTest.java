/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bcccp.carpark.entry;

import bcccp.carpark.CarSensor;
import bcccp.carpark.Carpark;
import bcccp.carpark.Gate;
import bcccp.carpark.ICarSensor;
import bcccp.carpark.ICarpark;
import bcccp.carpark.IGate;
import bcccp.tickets.adhoc.AdhocTicketDAO;
import bcccp.tickets.adhoc.AdhocTicketFactory;
import bcccp.tickets.adhoc.IAdhocTicket;
import bcccp.tickets.adhoc.IAdhocTicketDAO;
import bcccp.tickets.season.ISeasonTicketDAO;
import bcccp.tickets.season.SeasonTicketDAO;
import bcccp.tickets.season.UsageRecordFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.*;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class EntryControllerTest {

    EntryController myEntryController;
    ICarpark carPark;
    IGate entryGate;
    ICarSensor os;
    ICarSensor is;
    IEntryUI ui;
    IAdhocTicketDAO adhocTicketDAO;
    ISeasonTicketDAO seasonTicketDAO;

    public EntryControllerTest() {
    }

    @Before
    public void setUp() {
        carPark = mock(Carpark.class);
        entryGate = mock(Gate.class);
        os = mock(CarSensor.class);
        is = mock(CarSensor.class);
        ui = mock(EntryUI.class);
        adhocTicketDAO = spy(new AdhocTicketDAO(new AdhocTicketFactory()));
        seasonTicketDAO = spy(new SeasonTicketDAO(new UsageRecordFactory()));
        
        when(carPark.getName()).thenReturn("Test Car Park");
        
        myEntryController = new EntryController(carPark,entryGate,os,is,ui);
    }

    @After
    public void tearDown() {
        myEntryController = null;
        
        carPark = null;
        entryGate = null;
        os = null;
        is = null;
        ui = null;
    }

    @Test
    public void testButtonPushed() {
        System.out.println("buttonPushed");
        when(carPark.isFull()).thenReturn(true);
        myEntryController.buttonPushed();
        verify(ui, times(1)).display("Carpark Full");
        
        when(carPark.isFull()).thenReturn(false);
        IAdhocTicket adhocTicket = adhocTicketDAO.createTicket(carPark.getName());
        when(carPark.issueAdhocTicket()).thenReturn(adhocTicket);
        myEntryController.buttonPushed();
        verify(carPark, times(1)).issueAdhocTicket();
        verify(ui, times(1)).display("Take Ticket");
    }

    @Test
    public void testTicketInserted() {
        System.out.println("ticketInserted");
        String barcode = "S1234";
        when(carPark.isSeasonTicketValid(barcode)).thenReturn(true);
        when(carPark.isSeasonTicketInUse(barcode)).thenReturn(false);
        myEntryController.ticketInserted(barcode);
        verify(ui, times(1)).display("Take Ticket");
        
        when(carPark.isSeasonTicketValid(barcode)).thenReturn(false);
        myEntryController.ticketInserted(barcode);
        verify(ui, times(1)).display("Ticket Rejected");
    }

    @Test
    public void testTicketTaken() {
        System.out.println("ticketTaken");
        myEntryController.ticketTaken();
        verify(entryGate, times(0)).raise();
        
        
        //Make "isTicketValidated" to true
        String barcode = "S1234";
        when(carPark.isSeasonTicketValid(barcode)).thenReturn(true);
        when(carPark.isSeasonTicketInUse(barcode)).thenReturn(false);
        myEntryController.ticketInserted(barcode);
        
        myEntryController.ticketTaken();
        verify(ui,times(2)).display("");
        verify(entryGate, times(1)).raise();
    }

    @Test
    public void testNotifyCarparkEvent() {
        System.out.println("notifyCarparkEvent");
        
        //Verify that display("Push Button") should not be invoked
        myEntryController.notifyCarparkEvent();
        verify(ui, times(0)).display("Push Button");
        
        when(os.carIsDetected()).thenReturn(true);
        when(is.carIsDetected()).thenReturn(false);
        when(entryGate.isRaised()).thenReturn(false);
        
        //Verify that display("Push Button") should be invoked now
        myEntryController.notifyCarparkEvent();
        verify(ui, times(1)).display("Push Button");
    }

    @Test
    public void testCarEventDetectedOutsideSensor() {
        System.out.println("carEventDetected: OutsideSensor");
        String detectorId = "Entry Outside Sensor";
        boolean detected = true;
        myEntryController.carEventDetected(detectorId, detected);
        verify(ui, times(1)).display("Push Button");
        
        detected = false;
        myEntryController.setEnterMovingStatus("");
        myEntryController.carEventDetected(detectorId, detected);
        assertEquals("", myEntryController.getEnterMovingStatus());
        
        detected = false;
        myEntryController.setEnterMovingStatus("ENTERING");
        myEntryController.carEventDetected(detectorId, detected);
        assertEquals("ENTERED", myEntryController.getEnterMovingStatus());
        
    }
    
    @Test
    public void testCarEventDetectedInsideSensor() {
        System.out.println("carEventDetected: InsideSensor");
        String detectorId = "Entry Inside Sensor";
        
        boolean detected = true;
        myEntryController.setEnterMovingStatus("");
        myEntryController.carEventDetected(detectorId, detected);
        assertEquals("", myEntryController.getEnterMovingStatus());
        
        detected = true;
        myEntryController.setEnterMovingStatus("ISSUED");
        myEntryController.carEventDetected(detectorId, detected);
        assertEquals("ENTERING", myEntryController.getEnterMovingStatus());
        
        detected = false;
        myEntryController.setEnterMovingStatus("ENTERED");
        myEntryController.carEventDetected(detectorId, detected);
        assertEquals("", myEntryController.getEnterMovingStatus());
        verify(entryGate, times(1)).lower();
    }

}
