/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bcccp.carpark.exit;

import bcccp.carpark.CarSensor;
import bcccp.carpark.Carpark;
import bcccp.carpark.Gate;
import bcccp.carpark.ICarSensor;
import bcccp.carpark.ICarpark;
import bcccp.carpark.IGate;
import bcccp.carpark.paystation.PaystationController;
import bcccp.tickets.adhoc.AdhocTicketDAO;
import bcccp.tickets.adhoc.AdhocTicketFactory;
import bcccp.tickets.adhoc.IAdhocTicket;
import bcccp.tickets.adhoc.IAdhocTicketDAO;
import bcccp.tickets.season.ISeasonTicket;
import bcccp.tickets.season.ISeasonTicketDAO;
import bcccp.tickets.season.SeasonTicket;
import bcccp.tickets.season.SeasonTicketDAO;
import bcccp.tickets.season.UsageRecordFactory;
import java.util.Calendar;
import java.util.Date;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.*;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ExitControllerIT {
    //INTEGRATION TESTS OF EXIT CONTROLLER WITH CARPARK

    ExitController myExitController;
    ICarpark carPark;
    IGate exitGate;
    ICarSensor os;
    ICarSensor is;
    IExitUI ui;
    IAdhocTicket adhocTicket;
    IAdhocTicketDAO adhocTicketDAO;
    ISeasonTicketDAO seasonTicketDAO;
    String CarParkName = "Test Car Park";
    int CarParkCapacity = 3;
    String adhoctktBarcode = "";
    String seasonTktCode = "S1111";
    float adhocCharge = 2.0f;

    public ExitControllerIT() {
    }

    @Before
    public void setUp() {
        exitGate = mock(Gate.class);
        os = mock(CarSensor.class);
        is = mock(CarSensor.class);
        ui = mock(ExitUI.class);

        seasonTicketDAO = new SeasonTicketDAO(new UsageRecordFactory());
        adhocTicketDAO = new AdhocTicketDAO(new AdhocTicketFactory());
        carPark = new Carpark(CarParkName, CarParkCapacity, adhocTicketDAO, seasonTicketDAO);
        adhocTicket = carPark.issueAdhocTicket();
        adhoctktBarcode = adhocTicket.getBarcode();

        carPark.recordAdhocTicketEntry();
        long entryTime = (new Date()).getTime() - 5000 * 60;
        adhocTicket.enter(entryTime);

        Calendar cal1 = Calendar.getInstance();
        cal1.add(Calendar.DAY_OF_YEAR, -10);
        Calendar cal2 = Calendar.getInstance();
        cal2.add(Calendar.DAY_OF_YEAR, 10);
        ISeasonTicket t1 = new SeasonTicket(seasonTktCode, "Bathurst Chase", cal1.getTime().getTime(), cal2.getTime().getTime());
        carPark.registerSeasonTicket(t1);
        
        myExitController = new ExitController(carPark,exitGate,is,os,ui);
    }

    @After
    public void tearDown() {
        myExitController = null;
        carPark = null;
        adhocTicketDAO = null;
        seasonTicketDAO = null;
        adhocTicket = null;
    }

    @Test
    public void testTicketInsertedAdhoc() {
        System.out.println("Integration Test : ticketInserted : Adhoc");
        
        String ticketStr = "InvalidBarcde";
        myExitController.ticketInserted(ticketStr);
        verify(ui, times(1)).display("Take Rejected Ticket");        
        
        ticketStr = adhoctktBarcode;
        myExitController.ticketInserted(ticketStr);
        verify(ui, times(2)).display("Take Rejected Ticket"); 
        
        long paidTime = (new Date()).getTime();
        adhocTicket.pay(paidTime, adhocCharge);
        myExitController.ticketInserted(ticketStr);
        verify(ui, times(1)).display("Take Ticket");
    }
    
    @Test
    public void testTicketInsertedSeason() {
        System.out.println("Integration Test : ticketInserted : Season");
        
        myExitController.ticketInserted("InvalidCode");
        verify(ui, times(1)).display("Take Rejected Ticket"); 
                
        myExitController.ticketInserted(seasonTktCode);
        verify(ui, times(2)).display("Take Rejected Ticket");
        
        carPark.recordSeasonTicketEntry(seasonTktCode);
        myExitController.ticketInserted(seasonTktCode);
        verify(ui, times(1)).display("Take Ticket");
    }

    @Test
    public void testTicketTaken() {
        System.out.println("Integration Test : ticketTaken");        
        myExitController.ticketTaken();
        verify(exitGate, times(0)).raise();
        
        String ticketStr = adhoctktBarcode;
        long paidTime = (new Date()).getTime();
        adhocTicket.pay(paidTime, adhocCharge);
        myExitController.ticketInserted(ticketStr);
        myExitController.ticketTaken();
        verify(exitGate, times(1)).raise();
    }
}
