/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bcccp.carpark.exit;

import bcccp.carpark.CarSensor;
import bcccp.carpark.Carpark;
import bcccp.carpark.Gate;
import bcccp.carpark.ICarSensor;
import bcccp.carpark.ICarpark;
import bcccp.carpark.IGate;
import bcccp.tickets.adhoc.AdhocTicket;
import bcccp.tickets.adhoc.IAdhocTicket;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ExitControllerTest {
    ExitController myExitController;
    ICarpark carPark;
    IGate exitGate;
    ICarSensor os;
    ICarSensor is;
    IExitUI ui;
    IAdhocTicket adhocTicket;
    
    public ExitControllerTest() {
    }
    
    @Before
    public void setUp() {
        carPark = mock(Carpark.class);
        exitGate = mock(Gate.class);
        os = mock(CarSensor.class);
        is = mock(CarSensor.class);
        ui = mock(ExitUI.class);
        adhocTicket = mock(AdhocTicket.class); 
        
        myExitController = new ExitController(carPark,exitGate,is,os,ui);
    }
    
    @After
    public void tearDown() {
        myExitController = null;
        
        carPark = null;
        exitGate = null;
        os = null;
        is = null;
        ui = null;
    }

    @Test
    public void testTicketInsertedAdhoc() {
        System.out.println("ticketInserted : Adhoc");
        //For Adhoc tickets
        String ticketStr = "InvalidBarcde";
        when(carPark.getAdhocTicket(ticketStr)).thenReturn(null);
        myExitController.ticketInserted(ticketStr);
        verify(ui, times(1)).display("Take Rejected Ticket");        
        
        ticketStr = "A1221720171117";
        when(carPark.getAdhocTicket(ticketStr)).thenReturn(adhocTicket);
        when(adhocTicket.isPaid()).thenReturn(false);
        myExitController.ticketInserted(ticketStr);
        verify(ui, times(2)).display("Take Rejected Ticket"); 
        
        when(adhocTicket.isPaid()).thenReturn(true);
        myExitController.ticketInserted(ticketStr);
        verify(ui, times(1)).display("Take Ticket");
    }
    
    @Test
    public void testTicketInsertedSeason() {
        System.out.println("ticketInserted : Season"); 
        //For season tickets
        String ticketStr = "S1234";
        when(carPark.getAdhocTicket(ticketStr)).thenReturn(null);
        when(carPark.isSeasonTicketValid(ticketStr)).thenReturn(false);
        myExitController.ticketInserted(ticketStr);
        verify(ui, times(1)).display("Take Rejected Ticket"); 
        
        when(carPark.isSeasonTicketValid(ticketStr)).thenReturn(true);
        when(carPark.isSeasonTicketInUse(ticketStr)).thenReturn(false);
        myExitController.ticketInserted(ticketStr);
        verify(ui, times(2)).display("Take Rejected Ticket");
        
        when(carPark.isSeasonTicketInUse(ticketStr)).thenReturn(true);
        myExitController.ticketInserted(ticketStr);
        verify(ui, times(1)).display("Take Ticket");
    }

    @Test
    public void testTicketTaken() {
        System.out.println("ticketTaken");        
        myExitController.ticketTaken();
        verify(exitGate, times(0)).raise();
        
        String ticketStr = "A1221720171117";
        when(carPark.getAdhocTicket(ticketStr)).thenReturn(adhocTicket);
        when(adhocTicket.isPaid()).thenReturn(true);
        myExitController.ticketInserted(ticketStr);
        myExitController.ticketTaken();
        verify(exitGate, times(1)).raise();
    }

    @Test
    public void testCarEventDetectedInsideSensor() {
        System.out.println("carEventDetected: InsideSensor");
        String detectorId = "Exit Inside Sensor";
        boolean detected = true;
        myExitController.carEventDetected(detectorId, detected);
        verify(ui, times(1)).display("Insert Ticket");
        
        detected = false;
        myExitController.setExitMovingStatus("");
        myExitController.carEventDetected(detectorId, detected);
        assertEquals("", myExitController.getExitMovingStatus());
        
        detected = false;
        myExitController.setExitMovingStatus("LEAVING");
        myExitController.carEventDetected(detectorId, detected);
        assertEquals("LEFT", myExitController.getExitMovingStatus());
    }
    
    @Test
    public void testCarEventDetectedOutsideSensor() {
        System.out.println("carEventDetected: OutsideSensor");
        String detectorId = "Exit Outside Sensor";
        
        boolean detected = true;
        myExitController.setExitMovingStatus("");
        myExitController.carEventDetected(detectorId, detected);
        assertEquals("", myExitController.getExitMovingStatus());
        
        detected = true;
        myExitController.setExitMovingStatus("TAKEN");
        myExitController.carEventDetected(detectorId, detected);
        assertEquals("LEAVING", myExitController.getExitMovingStatus());
        
        detected = false;
        myExitController.setExitMovingStatus("LEFT");
        myExitController.carEventDetected(detectorId, detected);
        assertEquals("", myExitController.getExitMovingStatus());
        verify(exitGate, times(1)).lower();
    }
    
}
