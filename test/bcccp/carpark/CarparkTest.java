/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bcccp.carpark;

import bcccp.carpark.entry.EntryController;
import bcccp.carpark.entry.IEntryController;
import bcccp.tickets.adhoc.AdhocTicketDAO;
import bcccp.tickets.adhoc.AdhocTicketFactory;
import bcccp.tickets.adhoc.IAdhocTicket;
import bcccp.tickets.adhoc.IAdhocTicketDAO;
import bcccp.tickets.season.ISeasonTicket;
import bcccp.tickets.season.ISeasonTicketDAO;
import bcccp.tickets.season.SeasonTicket;
import bcccp.tickets.season.SeasonTicketDAO;
import bcccp.tickets.season.UsageRecordFactory;
import java.util.Calendar;
import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import static org.mockito.Mockito.*;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class CarparkTest {

    ICarpark myCarPark;
    IAdhocTicketDAO adhocTicketDAO;
    ISeasonTicketDAO seasonTicketDAO;
    ICarparkObserver observer;
    String CarParkName = "Test Car Park";
    int CarParkCapacity = 3;

    public CarparkTest() {

    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
//        adhocTicketDAO = new AdhocTicketDAO(new AdhocTicketFactory());
//	seasonTicketDAO = new SeasonTicketDAO(new UsageRecordFactory());
        adhocTicketDAO = spy(new AdhocTicketDAO(new AdhocTicketFactory()));
        seasonTicketDAO = spy(new SeasonTicketDAO(new UsageRecordFactory()));
        observer = mock(EntryController.class);
        myCarPark = new Carpark(CarParkName, CarParkCapacity, adhocTicketDAO, seasonTicketDAO);
    }

    @After
    public void tearDown() {
        adhocTicketDAO = null;
        seasonTicketDAO = null;
        myCarPark = null;
        observer = null;
    }

    /**
     * Test of register method, of class Carpark.
     */
    @Test
    public void testRegister() {
        System.out.println("register");
        ICarparkObserver observer = mock(EntryController.class);
        myCarPark.register(observer);
    }

    /**
     * Test of deregister method, of class Carpark.
     */
    @Test
    public void testDeregister() {
        System.out.println("deregister");
        ICarparkObserver observer = mock(EntryController.class);
        myCarPark.register(observer);
        myCarPark.deregister(observer);
    }

    /**
     * Test of getName method, of class Carpark.
     */
    @Test
    public void testGetName() {
        System.out.println("getName");
        String expResult = CarParkName;
        String result = myCarPark.getName();
        assertEquals(expResult, result);
    }

    /**
     * Test of isFull method, of class Carpark.
     */
    @Test
    public void testIsFull() {
        System.out.println("isFull");
        boolean expResult = false;
        boolean result = myCarPark.isFull();
        assertEquals(expResult, result);

        //Check if isfull returns true
        for (int i = 0; i < CarParkCapacity; i++) {
            myCarPark.recordAdhocTicketEntry();
        }
        expResult = true;
        result = myCarPark.isFull();
        assertEquals(expResult, result);
    }

    /**
     * Test of issueAdhocTicket method, of class Carpark.
     */
    @Test
    public void testIssueAdhocTicket() {
        System.out.println("issueAdhocTicket");
        assertTrue(myCarPark.issueAdhocTicket() instanceof IAdhocTicket);
        IAdhocTicket result = myCarPark.issueAdhocTicket();
        assertEquals(CarParkName, result.getCarparkId());
    }

    /**
     * Test of recordAdhocTicketEntry method, of class Carpark.
     */
    @Test
    public void testRecordAdhocTicketEntry() {
        System.out.println("recordAdhocTicketEntry");
        myCarPark.recordAdhocTicketEntry();

        //Check if recordAdhocTicketEntry is incrementing count or not by making the carpark full
        for (int i = 0; i < CarParkCapacity - 1; i++) {
            myCarPark.recordAdhocTicketEntry();
        }
        boolean expResult = true;
        boolean result = myCarPark.isFull();
        assertEquals(expResult, result);
    }

    /**
     * Test of getAdhocTicket method, of class Carpark.
     */
    @Test
    public void testGetAdhocTicket() {
        System.out.println("getAdhocTicket");
        IAdhocTicket ticket = myCarPark.issueAdhocTicket();
        String barcode = ticket.getBarcode();

        assertTrue(myCarPark.getAdhocTicket(barcode) instanceof IAdhocTicket);

        IAdhocTicket expResult = ticket;
        IAdhocTicket result = myCarPark.getAdhocTicket(barcode);
        assertEquals(expResult, result);
        assertEquals(null, myCarPark.getAdhocTicket("InvalidBarcode"));
    }

    /**
     * Test of calculateAddHocTicketCharge method, of class Carpark.
     */
    @Test
    public void testCalculateAddHocTicketCharge() {
        System.out.println("calculateAddHocTicketCharge");
        long entryDateTime = (new Date()).getTime() - 10 * 60 * 1000; // 10 mins before current time
        float result = myCarPark.calculateAddHocTicketCharge(entryDateTime);
        float expResult = 2.0F; // 10 min and $0.2 per minute

        assertEquals(expResult, result, 0.1);
    }

    /**
     * Test of recordAdhocTicketExit method, of class Carpark.
     */
    @Test
    public void testRecordAdhocTicketExit() {
        System.out.println("recordAdhocTicketExit");
        myCarPark.register(observer);
        ////make carpark full
        for (int i = 0; i < CarParkCapacity; i++) {
            myCarPark.recordAdhocTicketEntry();
        }

        myCarPark.recordAdhocTicketExit();
        //carpark should have space now
        boolean expResult = false;
        boolean result = myCarPark.isFull();
        assertEquals(expResult, result);

        //notifyCarparkEvent should have been invoked
        verify(observer).notifyCarparkEvent();
    }

    /**
     * Test of registerSeasonTicket method, of class Carpark.
     */
    @Test
    public void testRegisterSeasonTicket() {
        System.out.println("registerSeasonTicket");
        ISeasonTicket seasonTicket = mock(SeasonTicket.class);
        myCarPark.registerSeasonTicket(seasonTicket);
    }

    /**
     * Test of deregisterSeasonTicket method, of class Carpark.
     */
    @Test
    public void testDeregisterSeasonTicket() {
        System.out.println("deregisterSeasonTicket");
        ISeasonTicket seasonTicket = mock(SeasonTicket.class);
        myCarPark.deregisterSeasonTicket(seasonTicket);
    }

    /**
     * Test of isSeasonTicketValid method, of class Carpark.
     */
    @Test
    public void testIsSeasonTicketValid() {
        System.out.println("isSeasonTicketValid");
        //Create a valid season ticket for testing
        Calendar cal1 = Calendar.getInstance();
        cal1.add(Calendar.DAY_OF_YEAR, -10);
        long startTime = cal1.getTime().getTime();
        Calendar cal2 = Calendar.getInstance();
        cal2.add(Calendar.DAY_OF_YEAR, 10);
        long endTime = cal2.getTime().getTime();
        
        ISeasonTicket seasonTicket = mock(SeasonTicket.class);
        when(seasonTicket.getStartValidPeriod()).thenReturn(startTime);
        when(seasonTicket.getEndValidPeriod()).thenReturn(endTime);
        when(seasonTicket.getId()).thenReturn("S111");
        
        myCarPark.registerSeasonTicket(seasonTicket);
        String ticketId = seasonTicket.getId();

        boolean expResult = false;
        boolean result = myCarPark.isSeasonTicketValid("InvalidId");
        assertEquals(expResult, result);

        expResult = false;        
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        int hour = cal.get(Calendar.HOUR_OF_DAY);
        if (hour >= 9 && hour < 17) {
            expResult = true;
        }        
        result = myCarPark.isSeasonTicketValid(ticketId);
        assertEquals(expResult, result);
    }

    /**
     * Test of isSeasonTicketInUse method, of class Carpark.
     */
    @Test
    public void testIsSeasonTicketInUse() {
        System.out.println("isSeasonTicketInUse");
        ISeasonTicket seasonTicket = mock(SeasonTicket.class);
        when(seasonTicket.getId()).thenReturn("S111");
        when(seasonTicket.inUse()).thenReturn(true);
        myCarPark.registerSeasonTicket(seasonTicket);
        
        String ticketId = seasonTicket.getId();
        
        boolean expResult = true;
        boolean result = myCarPark.isSeasonTicketInUse(ticketId);
        assertEquals(expResult, result);
        
        expResult = false;
        result = myCarPark.isSeasonTicketValid("InvalidId");
        assertEquals(expResult, result);
    }

    /**
     * Test of recordSeasonTicketEntry method, of class Carpark.
     */
    @Test
    public void testRecordSeasonTicketEntry() {
        System.out.println("recordSeasonTicketEntry");
        ISeasonTicket seasonTicket = mock(SeasonTicket.class);
        when(seasonTicket.getId()).thenReturn("S111");
        myCarPark.registerSeasonTicket(seasonTicket);
        String ticketId = seasonTicket.getId();
        
        myCarPark.recordSeasonTicketEntry(ticketId);
        verify(seasonTicketDAO).recordTicketEntry(ticketId);
    }

    /**
     * Test of recordSeasonTicketExit method, of class Carpark.
     */
    @Test
    public void testRecordSeasonTicketExit() {
        System.out.println("recordSeasonTicketExit");
        ISeasonTicket seasonTicket = mock(SeasonTicket.class);
        when(seasonTicket.getId()).thenReturn("S111");
        myCarPark.registerSeasonTicket(seasonTicket);
        String ticketId = seasonTicket.getId();
        
        myCarPark.recordSeasonTicketExit(ticketId);
        verify(seasonTicketDAO).recordTicketExit(ticketId);
    }

}
