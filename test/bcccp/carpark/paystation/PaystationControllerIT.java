package bcccp.carpark.paystation;

import bcccp.carpark.Carpark;
import bcccp.carpark.ICarpark;
import bcccp.tickets.adhoc.AdhocTicketDAO;
import bcccp.tickets.adhoc.AdhocTicketFactory;
import bcccp.tickets.adhoc.IAdhocTicket;
import bcccp.tickets.adhoc.IAdhocTicketDAO;
import bcccp.tickets.season.ISeasonTicketDAO;
import bcccp.tickets.season.SeasonTicketDAO;
import java.util.Date;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class PaystationControllerIT {
    //INTEGRATION TESTS OF PAYSTATION WITH ADHOC TICKETS and carpark

    PaystationController myPaystationController;
    ICarpark carPark;
    IPaystationUI ui;
    IAdhocTicketDAO adhocTicketDAO;
    IAdhocTicket adhocTicket;
    ISeasonTicketDAO seasonTicketDAO;
    String CarParkName = "Test Car Park";
    int CarParkCapacity = 3;
    String tktBarcode = "";

    public PaystationControllerIT() {
    }

    @Before
    public void setUp() {
        seasonTicketDAO = mock(SeasonTicketDAO.class);
        ui = mock(PaystationUI.class);
        adhocTicketDAO = new AdhocTicketDAO(new AdhocTicketFactory());
        carPark = new Carpark(CarParkName, CarParkCapacity, adhocTicketDAO, seasonTicketDAO);
        adhocTicket = carPark.issueAdhocTicket();
        tktBarcode = adhocTicket.getBarcode();
        myPaystationController = new PaystationController(carPark, ui);

        carPark.recordAdhocTicketEntry();
        long entryTime = (new Date()).getTime() - 5000 * 60;
        adhocTicket.enter(entryTime);
    }

    @After
    public void tearDown() {
        myPaystationController = null;
        adhocTicketDAO = null;
        carPark = null;
    }

    @Test
    public void testTicketInserted() {
        System.out.println("Integration Test : ticketInserted");
        String barcode = "InvalidBarcode";
        myPaystationController.ticketInserted(barcode);
        verify(ui, times(1)).display("Rejected");

        barcode = tktBarcode;
        myPaystationController.ticketInserted(barcode);
        float expectedCharge = (float) (0.2 * (((new Date()).getTime() - adhocTicket.getEntryDateTime()) / 1000) / 60);
        verify(ui, times(1)).display("$" + expectedCharge);
    }

    @Test
    public void testTicketPaid() {
        System.out.println("Integration Test : ticketPaid");

        String barcode = tktBarcode;
        myPaystationController.ticketInserted(barcode);
        myPaystationController.ticketPaid();
        verify(ui, times(1)).display("Take Ticket");
    }

    @Test
    public void testTicketTaken() {
        System.out.println("Integration Test : ticketTaken");
        String barcode = tktBarcode;
        myPaystationController.ticketInserted(barcode);
        myPaystationController.ticketPaid();
        myPaystationController.ticketTaken();
        verify(ui, times(1)).display("");
    }

}
