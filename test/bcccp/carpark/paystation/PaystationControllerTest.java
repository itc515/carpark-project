/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bcccp.carpark.paystation;

import bcccp.carpark.Carpark;
import bcccp.carpark.ICarpark;
import bcccp.tickets.adhoc.AdhocTicket;
import bcccp.tickets.adhoc.IAdhocTicket;
import java.util.Date;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class PaystationControllerTest {
    PaystationController myPaystationController;
    ICarpark carPark;
    IPaystationUI ui;
    IAdhocTicket adhocTicket;
    
    public PaystationControllerTest() {
    }
    
    @Before
    public void setUp() {
        carPark = mock(Carpark.class);
        ui = mock(PaystationUI.class);
        adhocTicket = mock(AdhocTicket.class);       
        
        myPaystationController = new PaystationController(carPark,ui);
    }
    
    @After
    public void tearDown() {
        myPaystationController = null;
        ui = null;
        carPark = null;
    }

    @Test
    public void testTicketInserted() {
        System.out.println("ticketInserted");
        
        
        String barcode = "InvalidBarcode";     
        when(carPark.getAdhocTicket(barcode)).thenReturn(null);
        myPaystationController.ticketInserted(barcode);
        verify(ui, times(1)).display("Rejected");
        
        barcode = "A1201720141117";
        when(carPark.getAdhocTicket(barcode)).thenReturn(adhocTicket);
        long entryDateTime = (new Date()).getTime() - 2*60*1000;
        when(adhocTicket.getEntryDateTime()).thenReturn(entryDateTime);
        when(carPark.calculateAddHocTicketCharge(entryDateTime)).thenReturn(0.4f);
        myPaystationController.ticketInserted(barcode);
        verify(ui, times(1)).display("$0.4");
    }

    @Test
    public void testTicketPaid() {
        System.out.println("ticketPaid");
        
        String barcode = "A2017090909";
        when(carPark.getAdhocTicket(barcode)).thenReturn(adhocTicket);
        long entryDateTime = (new Date()).getTime() - 2*60*1000;
        when(adhocTicket.getEntryDateTime()).thenReturn(entryDateTime);
        when(carPark.calculateAddHocTicketCharge(entryDateTime)).thenReturn(0.4f);
        myPaystationController.ticketInserted(barcode);
        
        myPaystationController.ticketPaid();
        verify(ui, times(1)).display("Take Ticket");
    }

    @Test
    public void testTicketTaken() {
        System.out.println("ticketTaken");        
        myPaystationController.ticketTaken();
        verify(ui, times(1)).display("");
    }
    
}
