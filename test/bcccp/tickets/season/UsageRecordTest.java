/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bcccp.tickets.season;

import java.util.Calendar;
import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.mockito.Mockito.*;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class UsageRecordTest {
    IUsageRecord myUsageRecord;
    String ticketId = "S1234";
    String carparkId = "Test Car Park";
    long startDateTime;
    long endDateTime;
    
    public UsageRecordTest() {
    } 
    
    @Before
    public void setUp() {
        startDateTime = (new Date()).getTime();
        endDateTime = (new Date()).getTime() + 2000;
        
        myUsageRecord = new UsageRecord(ticketId, startDateTime);
    }
    
    @After
    public void tearDown() {
        myUsageRecord = null;
    }

    @Test
    public void testFinalise() {
        System.out.println("finalise");
        
        myUsageRecord.finalise(endDateTime);
        
        long result = myUsageRecord.getEndTime();
        assertEquals(endDateTime, result);
    }

    @Test
    public void testGetStartTime() {
        System.out.println("getStartTime");
        
        long expResult = startDateTime;
        long result = myUsageRecord.getStartTime();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetEndTime() {
        System.out.println("getEndTime");
        
        long expResult = 0L;
        long result = myUsageRecord.getEndTime();
        assertEquals(expResult, result);
        
        myUsageRecord.finalise(endDateTime);        
        result = myUsageRecord.getEndTime();
        assertEquals(endDateTime, result);
    }

    @Test
    public void testGetSeasonTicketId() {
        System.out.println("getSeasonTicketId");        
        String expResult = ticketId;
        String result = myUsageRecord.getSeasonTicketId();
        assertEquals(expResult, result);
    }    
}
