/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bcccp.tickets.season;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.mockito.Mockito.*;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class SeasonTicketTest {

    ISeasonTicket mySeasonTicket;
    String ticketId = "S1234";
    String carparkId = "Test Car Park";
    long startValidPeriod;
    long endValidPeriod;

    public SeasonTicketTest() {
    }

    @Before
    public void setUp() {
        Calendar cal1 = Calendar.getInstance();
        cal1.add(Calendar.DAY_OF_YEAR, -10);
        startValidPeriod = cal1.getTime().getTime();
        Calendar cal2 = Calendar.getInstance();
        cal2.add(Calendar.DAY_OF_YEAR, 10);
        endValidPeriod = cal2.getTime().getTime();

        mySeasonTicket = new SeasonTicket(ticketId, carparkId, startValidPeriod, endValidPeriod);
    }

    @After
    public void tearDown() {
        mySeasonTicket = null;
    }

    @Test
    public void testGetId() {
        System.out.println("getId");        
        String expResult = ticketId;
        String result = mySeasonTicket.getId();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetCarparkId() {
        System.out.println("getCarparkId");        
        String expResult = carparkId;
        String result = mySeasonTicket.getCarparkId();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetStartValidPeriod() {
        System.out.println("getStartValidPeriod");        
        long expResult = startValidPeriod;
        long result = mySeasonTicket.getStartValidPeriod();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetEndValidPeriod() {
        System.out.println("getEndValidPeriod");        
        long expResult = endValidPeriod;
        long result = mySeasonTicket.getEndValidPeriod();
        assertEquals(expResult, result);
    }

    @Test
    public void testInUse() {
        System.out.println("inUse");        
        boolean expResult = false;
        boolean result = mySeasonTicket.inUse();
        assertEquals(expResult, result);
        
        IUsageRecord record = mock(UsageRecord.class);
        mySeasonTicket.recordUsage(record);
        expResult = true;
        result = mySeasonTicket.inUse();
        assertEquals(expResult, result);
    }

    @Test
    public void testRecordUsage() {
        System.out.println("recordUsage");
        IUsageRecord record = mock(UsageRecord.class);
        mySeasonTicket.recordUsage(record); 
        
        IUsageRecord result = mySeasonTicket.getCurrentUsageRecord();
        assertEquals(record, result);
    }

    @Test
    public void testGetCurrentUsageRecord() {
        System.out.println("getCurrentUsageRecord");
        IUsageRecord record = mock(UsageRecord.class);
        mySeasonTicket.recordUsage(record);
        IUsageRecord result = mySeasonTicket.getCurrentUsageRecord();
        assertEquals(record, result);
    }

    @Test
    public void testEndUsage() {
        System.out.println("endUsage");
        IUsageRecord record = mock(UsageRecord.class);
        mySeasonTicket.recordUsage(record);
        
        long endTime = (new Date()).getTime() + 2*1000;        
        mySeasonTicket.endUsage(endTime);
        assertEquals(null, mySeasonTicket.getCurrentUsageRecord());
    }

    @Test
    public void testGetUsageRecords() {
        System.out.println("getUsageRecords");
        
        assertTrue(mySeasonTicket.getUsageRecords() instanceof List);
        
        List<IUsageRecord> result = mySeasonTicket.getUsageRecords();
        assertEquals(0, result.size());
        
        IUsageRecord record = mock(UsageRecord.class);
        mySeasonTicket.recordUsage(record);
        
        result = mySeasonTicket.getUsageRecords();
        assertEquals(1, result.size());
    }
}
