/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bcccp.tickets.season;

import java.util.Date;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author mfaiz
 */
public class UsageRecordFactoryTest {
    IUsageRecordFactory myUsageRecordFactory;
    
    public UsageRecordFactoryTest() {
    }
    
    @Before
    public void setUp() {
        myUsageRecordFactory = new UsageRecordFactory();
    }
    
    @After
    public void tearDown() {
        myUsageRecordFactory = null;
    }

    @Test
    public void testMake() {
        System.out.println("make");
        String ticketId = "s1234";
        long startDateTime = (new Date()).getTime();
        
        IUsageRecord result = myUsageRecordFactory.make(ticketId, startDateTime);
        assertEquals(ticketId, result.getSeasonTicketId());
        assertEquals(startDateTime, result.getStartTime());
    }
    
}
