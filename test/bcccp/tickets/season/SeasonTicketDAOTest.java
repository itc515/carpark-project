/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bcccp.tickets.season;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.*;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class SeasonTicketDAOTest {
    ISeasonTicketDAO mySeasonTicketDAO;
    String ticketId = "S1234";
    
    public SeasonTicketDAOTest() {
    }
    
    @Before
    public void setUp() {
        IUsageRecordFactory usageRecordFactory = spy(new UsageRecordFactory());
        mySeasonTicketDAO = new SeasonTicketDAO(usageRecordFactory);
    }
    
    @After
    public void tearDown() {
        mySeasonTicketDAO = null;
    }

    @Test
    public void testRegisterTicket() {
        System.out.println("registerTicket");
        ISeasonTicket ticket = mock(SeasonTicket.class);        
        mySeasonTicketDAO.registerTicket(ticket);
    }

    @Test
    public void testDeregisterTicket() {
        System.out.println("deregisterTicket");
        ISeasonTicket ticket = mock(SeasonTicket.class); 
        mySeasonTicketDAO.registerTicket(ticket);
        mySeasonTicketDAO.deregisterTicket(ticket);
    }

    @Test
    public void testGetNumberOfTickets() {
        System.out.println("getNumberOfTickets");
        
        int expResult = 0;
        int result = mySeasonTicketDAO.getNumberOfTickets();
        assertEquals(expResult, result);
        
        ISeasonTicket ticket = mock(SeasonTicket.class);        
        mySeasonTicketDAO.registerTicket(ticket);
        
        expResult = 1;
        result = mySeasonTicketDAO.getNumberOfTickets();
        assertEquals(expResult, result);
    }

    @Test
    public void testFindTicketById() {
        System.out.println("findTicketById");
        ISeasonTicket ticket = mock(SeasonTicket.class);  
        when(ticket.getId()).thenReturn(ticketId);
        
        mySeasonTicketDAO.registerTicket(ticket);
        
        ISeasonTicket expResult = null;
        ISeasonTicket result = mySeasonTicketDAO.findTicketById("InvalidId");
        assertEquals(expResult, result);
        
        expResult = ticket;
        result = mySeasonTicketDAO.findTicketById(ticketId);
        assertEquals(expResult, result);
    }

    @Test
    public void testRecordTicketEntry() {
        System.out.println("recordTicketEntry");        
        mySeasonTicketDAO.recordTicketEntry(ticketId);
    }

    @Test
    public void testRecordTicketExit() {
        System.out.println("recordTicketExit");        
        mySeasonTicketDAO.recordTicketExit(ticketId);
    }
    
}
