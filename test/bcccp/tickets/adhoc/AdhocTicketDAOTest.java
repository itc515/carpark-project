/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bcccp.tickets.adhoc;

import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import static org.mockito.Mockito.spy;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class AdhocTicketDAOTest {
    IAdhocTicketDAO myAdhocTicketDAO;
    String carparkId = "Test Car Park";
    
    public AdhocTicketDAOTest() {
    }
    
    @Before
    public void setUp() {
        IAdhocTicketFactory adhocTicketFactory = spy(new AdhocTicketFactory());
        myAdhocTicketDAO = new AdhocTicketDAO(adhocTicketFactory);
    }
    
    @After
    public void tearDown() {
        myAdhocTicketDAO = null;
    }

    @Test
    public void testCreateTicket() {
        System.out.println("createTicket");
        String carparkId = "";
        assertTrue(myAdhocTicketDAO.createTicket(carparkId) instanceof IAdhocTicket);
        
        IAdhocTicket result = myAdhocTicketDAO.createTicket(carparkId);
        assertEquals(carparkId, result.getCarparkId());
    }

    @Test
    public void testFindTicketByBarcode() {
        System.out.println("findTicketByBarcode");
        IAdhocTicket ticket = myAdhocTicketDAO.createTicket(carparkId);
        
        String barcode = ticket.getBarcode();
        
        IAdhocTicket expResult = null;
        IAdhocTicket result = myAdhocTicketDAO.findTicketByBarcode("InvalidBarcode");
        assertEquals(expResult, result);
        
        expResult = ticket;
        result = myAdhocTicketDAO.findTicketByBarcode(barcode);
        assertEquals(expResult, result);
    }

    @Test
    public void testGetCurrentTickets() {
        System.out.println("getCurrentTickets");
        
        assertTrue(myAdhocTicketDAO.getCurrentTickets() instanceof List);
        
        List<IAdhocTicket> result = myAdhocTicketDAO.getCurrentTickets();
        assertEquals(0, result.size());
        
        IAdhocTicket ticket = myAdhocTicketDAO.createTicket(carparkId);
        result = myAdhocTicketDAO.getCurrentTickets();
        assertEquals(1, result.size());
    }
    
}
