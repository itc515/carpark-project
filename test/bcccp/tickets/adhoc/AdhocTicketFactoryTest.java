/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bcccp.tickets.adhoc;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.mockito.Mockito.*;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class AdhocTicketFactoryTest {
    IAdhocTicketFactory myAdhocTicketFactory;    
    String carparkId = "Test Car Park";

    
    public AdhocTicketFactoryTest() {
    }
    
    @Before
    public void setUp() {
        myAdhocTicketFactory = new AdhocTicketFactory();
    }
    
    @After
    public void tearDown() {
        myAdhocTicketFactory = null;
    }

    @Test
    public void testMake() {
        System.out.println("make");
        int ticketNo = 0;
        
        assertTrue(myAdhocTicketFactory.make(carparkId, ticketNo) instanceof IAdhocTicket);
        
        ticketNo = 1;
        IAdhocTicket result = myAdhocTicketFactory.make(carparkId, ticketNo);
        assertEquals(carparkId, result.getCarparkId());
        assertEquals(ticketNo, result.getTicketNo());
    }
    
}
