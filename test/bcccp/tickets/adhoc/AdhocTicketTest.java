/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bcccp.tickets.adhoc;

import java.text.SimpleDateFormat;
import java.util.Date;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.*;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class AdhocTicketTest {

    IAdhocTicket myAdhocTicket;
    String carparkId = "Test Car Park";
    int ticketNo = 1;
    String barcode;

    public AdhocTicketTest() {
    }

    @Before
    public void setUp() {
        barcode = "" + ticketNo + (new SimpleDateFormat("ddmmyyyyHHmm")).format(new Date());
        myAdhocTicket = new AdhocTicket(carparkId, ticketNo, barcode);
    }

    @After
    public void tearDown() {
        barcode = "";
        myAdhocTicket = null;
    }

    @Test
    public void testGetTicketNo() {
        System.out.println("getTicketNo");
        int expResult = ticketNo;
        int result = myAdhocTicket.getTicketNo();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetBarcode() {
        System.out.println("getBarcode");
        String expResult = barcode;
        String result = myAdhocTicket.getBarcode();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetCarparkId() {
        System.out.println("getCarparkId");
        String expResult = carparkId;
        String result = myAdhocTicket.getCarparkId();
        assertEquals(expResult, result);
    }

    @Test
    public void testEnter() {
        System.out.println("enter");
        long dateTime = (new Date()).getTime();
        myAdhocTicket.enter(dateTime);
        
        assertEquals(dateTime, myAdhocTicket.getEntryDateTime());
    }

    @Test
    public void testGetEntryDateTime() {
        System.out.println("getEntryDateTime");
        
        long result = myAdhocTicket.getEntryDateTime();
        assertEquals(0.0, result,0.0);
        
        long expResult = (new Date()).getTime();
        myAdhocTicket.enter(expResult);
        result = myAdhocTicket.getEntryDateTime();
        assertEquals(expResult, result);
    }

    @Test
    public void testIsCurrent() {
        System.out.println("isCurrent");
        boolean expResult = false;
        boolean result = myAdhocTicket.isCurrent();
        assertEquals(expResult, result);
        
        long dateTime = (new Date()).getTime();
        myAdhocTicket.enter(dateTime);
        expResult = true;
        result = myAdhocTicket.isCurrent();
        assertEquals(expResult, result);
        
        dateTime = (new Date()).getTime();
        myAdhocTicket.exit(dateTime);
        expResult = false;
        result = myAdhocTicket.isCurrent();
        assertEquals(expResult, result);
    }

    @Test
    public void testPay() {
        System.out.println("pay");
        long dateTime = (new Date()).getTime();
        myAdhocTicket.enter(dateTime);
        float charge = 2.0F;
        
        long paidTime = (new Date()).getTime();
        myAdhocTicket.pay(paidTime, charge);
        
        assertEquals(paidTime, myAdhocTicket.getPaidDateTime());
        assertEquals(charge, myAdhocTicket.getCharge(),0.0);
    }

    @Test
    public void testGetPaidDateTime() {
        System.out.println("getPaidDateTime");
        long dateTime = (new Date()).getTime();
        myAdhocTicket.enter(dateTime);
        
        assertEquals(0.0, myAdhocTicket.getPaidDateTime(), 0.0);
        
        float charge = 2.0F;        
        long paidTime = (new Date()).getTime();
        myAdhocTicket.pay(paidTime, charge);        
        assertEquals(paidTime, myAdhocTicket.getPaidDateTime());
    }

    @Test
    public void testIsPaid() {
        System.out.println("isPaid");
        boolean expResult = false;
        boolean result = myAdhocTicket.isPaid();
        assertEquals(expResult, result);
        
        long dateTime = (new Date()).getTime();
        myAdhocTicket.enter(dateTime);
        float charge = 2.0F;
        
        long paidTime = (new Date()).getTime();
        myAdhocTicket.pay(paidTime, charge);
        
        expResult = true;
        result = myAdhocTicket.isPaid();
        assertEquals(expResult, result);       
    }

    @Test
    public void testGetCharge() {
        System.out.println("getCharge");
        long dateTime = (new Date()).getTime();
        myAdhocTicket.enter(dateTime);
        
        assertEquals(0.0, myAdhocTicket.getCharge(),0.0);
        
        float charge = 2.0F;        
        long paidTime = (new Date()).getTime();
        myAdhocTicket.pay(paidTime, charge);        
        assertEquals(charge, myAdhocTicket.getCharge(),0.0);
    }

    @Test
    public void testExit() {
        System.out.println("exit");
        long dateTime = (new Date()).getTime();
        myAdhocTicket.exit(dateTime);
        
        assertEquals(dateTime, myAdhocTicket.getExitDateTime());
    }

    @Test
    public void testGetExitDateTime() {
        System.out.println("getExitDateTime");
        
        assertEquals(0.0, myAdhocTicket.getExitDateTime(),0.0);
        
        long dateTime = (new Date()).getTime();
        myAdhocTicket.exit(dateTime);
        
        assertEquals(dateTime, myAdhocTicket.getExitDateTime());
    }

    @Test
    public void testHasExited() {
        System.out.println("hasExited");
        boolean expResult = false;
        boolean result = myAdhocTicket.hasExited();
        assertEquals(expResult, result);
        
        long dateTime = (new Date()).getTime();
        myAdhocTicket.exit(dateTime);        
        expResult = true;
        result = myAdhocTicket.hasExited();
        assertEquals(expResult, result);
    }

}
