package bcccp.carpark.exit;

import bcccp.carpark.Carpark;
import bcccp.carpark.ICarSensor;
import bcccp.carpark.ICarSensorResponder;
import bcccp.carpark.ICarpark;
import bcccp.carpark.IGate;
import bcccp.tickets.adhoc.IAdhocTicket;
import java.util.Date;

public class ExitController
        implements ICarSensorResponder,
        IExitController {

    private IGate exitGate;
    private ICarSensor insideSensor;
    private ICarSensor outsideSensor;
    private IExitUI ui;

    private ICarpark carpark;
    private IAdhocTicket adhocTicket = null;
    private long exitTime;
    private String seasonTicketId = "";

    private String exitMovingStatus = "";
    private Boolean isTicketValidated = false;

    public ExitController(ICarpark carpark, IGate exitGate,
            ICarSensor is,
            ICarSensor os,
            IExitUI ui) {
        //TODO Implement constructor
        this.carpark = carpark;
        this.exitGate = exitGate;
        this.insideSensor = is;
        this.outsideSensor = os;
        this.ui = ui;

        this.outsideSensor.registerResponder(this);
        this.insideSensor.registerResponder(this);

        ui.registerController(this);
    }

    public String getExitMovingStatus() {
        return exitMovingStatus;
    }

    public void setExitMovingStatus(String exitMovingStatusValue) {
        this.exitMovingStatus = exitMovingStatusValue;
    }

    @Override
    public void ticketInserted(String ticketStr) {
        // TODO Auto-generated method stub
        adhocTicket = carpark.getAdhocTicket(ticketStr);
        if (adhocTicket != null && adhocTicket.isPaid()) {
            this.ui.display("Take Ticket");
            isTicketValidated = true;
            return;
        }

        if (carpark.isSeasonTicketValid(ticketStr)) {
            if (carpark.isSeasonTicketInUse(ticketStr)) {
                seasonTicketId = ticketStr;
                this.ui.display("Take Ticket");
                isTicketValidated = true;
                return;
            }
        }

        this.ui.display("Take Rejected Ticket");
        isTicketValidated = false;
    }

    @Override
    public void ticketTaken() {
        // TODO Auto-generated method stub
        if (isTicketValidated) {
            exitGate.raise();
            exitMovingStatus = "TAKEN";
            isTicketValidated = false;
        }

        ui.display("");
    }

    @Override
    public void carEventDetected(String detectorId, boolean detected) {
        // TODO Auto-generated method stub
        if (detectorId.contentEquals("Exit Inside Sensor")) {
            if (detected) {
                ui.display("Insert Ticket");
            } else {
                ui.display("");

                if (exitMovingStatus.contentEquals("LEAVING")) {
                    exitMovingStatus = "LEFT";
                }
            }
        } else if (detectorId.contentEquals("Exit Outside Sensor")) {
            if (detected) {
                if (exitMovingStatus.contentEquals("TAKEN")) {
                    exitMovingStatus = "LEAVING";
                }
            } else {
                if (exitMovingStatus.contentEquals("LEFT")) {
                    exitMovingStatus = "";
                    this.exitGate.lower();

                    if (adhocTicket != null) {
                        this.carpark.recordAdhocTicketExit();
                        Date dte = new Date();
                        adhocTicket.exit(dte.getTime());
                    } else if (!seasonTicketId.isEmpty()) {
                        carpark.recordSeasonTicketExit(seasonTicketId);
                    }

                    seasonTicketId = "";
                    adhocTicket = null;
                    isTicketValidated = false;
                }
            }
        }
    }

}
