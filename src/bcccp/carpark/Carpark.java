package bcccp.carpark;

import java.util.List;

import bcccp.tickets.adhoc.IAdhocTicket;
import bcccp.tickets.adhoc.IAdhocTicketDAO;
import bcccp.tickets.season.ISeasonTicket;
import bcccp.tickets.season.ISeasonTicketDAO;
import java.util.ArrayList;
import java.util.Date;
import java.util.Calendar;

public class Carpark implements ICarpark {

    private List<ICarparkObserver> observers;
    private String carparkId;
    private int capacity;
    private int numberOfCarsParked;
    private IAdhocTicketDAO adhocTicketDAO;
    private ISeasonTicketDAO seasonTicketDAO;

    public Carpark(String name, int capacity,
            IAdhocTicketDAO adhocTicketDAO,
            ISeasonTicketDAO seasonTicketDAO) {
        //TODO Implement constructor
        this.carparkId = name;
        this.capacity = capacity;
        this.adhocTicketDAO = adhocTicketDAO;
        this.seasonTicketDAO = seasonTicketDAO;
        this.numberOfCarsParked = 0;

        observers = new ArrayList<ICarparkObserver>();
    }

    @Override
    public void register(ICarparkObserver observer) {
        if (!observers.contains(observer)) {
            observers.add(observer);
        }
    }

    @Override
    public void deregister(ICarparkObserver observer) {
        if (observers.contains(observer)) {
            observers.remove(observer);
        }
    }

    @Override
    public String getName() {
        return this.carparkId;
    }

    @Override
    public boolean isFull() {
        return numberOfCarsParked >= capacity ? true : false;
    }

    @Override
    public IAdhocTicket issueAdhocTicket() {
        return adhocTicketDAO.createTicket(carparkId);
    }

    @Override
    public void recordAdhocTicketEntry() {
        numberOfCarsParked++;
    }

    @Override
    public IAdhocTicket getAdhocTicket(String barcode) {
        return adhocTicketDAO.findTicketByBarcode(barcode);
    }

    @Override
    public float calculateAddHocTicketCharge(long entryDateTime) {
        Date dte = new Date();
        long paidTime = dte.getTime();
        long secs = (paidTime - entryDateTime) / 1000;
        long mins = secs / 60;
        return (float) (mins * 0.20); //Assuming $0.20 per minute
    }

    @Override
    public void recordAdhocTicketExit() {
        Boolean wasFull = false;
        if (numberOfCarsParked >= capacity) {
            wasFull = true;
        }
        numberOfCarsParked--;

        //Notify observers if wasFull
        if (wasFull) {
            for (ICarparkObserver observer : observers) {
                observer.notifyCarparkEvent();
            }
        }
    }

    @Override
    public void registerSeasonTicket(ISeasonTicket seasonTicket) {
        seasonTicketDAO.registerTicket(seasonTicket);
    }

    @Override
    public void deregisterSeasonTicket(ISeasonTicket seasonTicket) {
        seasonTicketDAO.deregisterTicket(seasonTicket);
    }

    @Override
    public boolean isSeasonTicketValid(String ticketId) {
        ISeasonTicket ticket = seasonTicketDAO.findTicketById(ticketId);
        if(ticket == null)
            return false;
        
        long startDate = ticket.getStartValidPeriod();
        long endDate = ticket.getEndValidPeriod();
        Date dte = new Date();
        long currentTime = dte.getTime();

        //Valid if current time lies within ticket valid time and within working hours (9 AM - 5 PM)
        if (startDate <= currentTime && currentTime <= endDate) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(dte);  
            int hour = cal.get(Calendar.HOUR_OF_DAY);
            if(hour >= 9 && hour < 17){
                return true;
            }            
        }

        return false;
    }

    @Override
    public boolean isSeasonTicketInUse(String ticketId) {
        ISeasonTicket ticket = seasonTicketDAO.findTicketById(ticketId);
        return ticket.inUse();
    }

    @Override
    public void recordSeasonTicketEntry(String ticketId) {
        seasonTicketDAO.recordTicketEntry(ticketId);
    }

    @Override
    public void recordSeasonTicketExit(String ticketId) {
        seasonTicketDAO.recordTicketExit(ticketId);
    }
}