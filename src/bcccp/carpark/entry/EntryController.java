package bcccp.carpark.entry;

import bcccp.carpark.Carpark;
import bcccp.carpark.ICarSensor;
import bcccp.carpark.ICarSensorResponder;
import bcccp.carpark.ICarpark;
import bcccp.carpark.ICarparkObserver;
import bcccp.carpark.IGate;
import bcccp.tickets.adhoc.IAdhocTicket;
import java.util.Date;

public class EntryController
        implements ICarSensorResponder,
        ICarparkObserver,
        IEntryController {

    private IGate entryGate;
    private ICarSensor outsideSensor;
    private ICarSensor insideSensor;
    private IEntryUI ui;

    private ICarpark carpark;
    private IAdhocTicket adhocTicket = null;
    private long entryTime;
    private String seasonTicketId = "";

    private String enterMovingStatus = "";
    private Boolean isTicketValidated = false;
    
    public String getEnterMovingStatus(){
        return enterMovingStatus;
    }
    public void setEnterMovingStatus(String enterMovingStatusValue){
        this.enterMovingStatus = enterMovingStatusValue;
    }

    public EntryController(ICarpark carpark, IGate entryGate,
            ICarSensor os,
            ICarSensor is,
            IEntryUI ui) {
        this.carpark = carpark;
        this.entryGate = entryGate;
        this.outsideSensor = os;
        this.insideSensor = is;
        this.ui = ui;

        this.outsideSensor.registerResponder(this);
        this.insideSensor.registerResponder(this);

        this.ui.registerController(this);
        this.carpark.register(this);
    }

    @Override
    public void buttonPushed() {
        if (this.carpark.isFull()) {
            this.ui.display("Carpark Full");
            return;
        }

        this.adhocTicket = this.carpark.issueAdhocTicket();
        String carparkID = this.carpark.getName();
        entryTime = (new Date()).getTime();
        this.ui.printTicket(carparkID, this.adhocTicket.getTicketNo(), entryTime, this.adhocTicket.getBarcode());
        this.ui.display("Take Ticket");
        isTicketValidated = true;
    }

    @Override
    public void ticketInserted(String barcode) {
        if (carpark.isSeasonTicketValid(barcode)) {
            if (!carpark.isSeasonTicketInUse(barcode)) {
                seasonTicketId = barcode;
                this.ui.display("Take Ticket");
                isTicketValidated = true;
                return;
            }
        }

        this.ui.display("Ticket Rejected");
    }

    @Override
    public void ticketTaken() {
        this.ui.display("");
        if(isTicketValidated){
            this.entryGate.raise();
            enterMovingStatus = "ISSUED";
            isTicketValidated = false;
        }        
    }

    @Override
    public void notifyCarparkEvent() {
        if (outsideSensor.carIsDetected() && !entryGate.isRaised() && !insideSensor.carIsDetected()) {
            ui.display("Push Button");
        }
    }

    @Override
    public void carEventDetected(String detectorId, boolean detected) {
        if (detectorId.contentEquals("Entry Outside Sensor")) {
            if (detected) {
                ui.display("Push Button");
            } else {
                ui.display("");

                if (enterMovingStatus.contentEquals("ENTERING")) {
                    enterMovingStatus = "ENTERED";
                }
            }
        } else if (detectorId.contentEquals("Entry Inside Sensor")) {
            if (detected) {
                if (enterMovingStatus.contentEquals("ISSUED")) {
                    enterMovingStatus = "ENTERING";
                }
            } else {
                if (enterMovingStatus.contentEquals("ENTERED")) {
                    enterMovingStatus = "";
                    this.entryGate.lower();

                    if (adhocTicket != null) {
                        this.carpark.recordAdhocTicketEntry();
                        adhocTicket.enter(entryTime);
                    } else if (!seasonTicketId.isEmpty()) {
                        carpark.recordSeasonTicketEntry(seasonTicketId);
                    }

                    seasonTicketId = "";
                    adhocTicket = null;
                    isTicketValidated = false;
                    entryTime = 0;
                }
            }
        }
    }
}