package bcccp.carpark.paystation;

import bcccp.carpark.ICarpark;
import bcccp.tickets.adhoc.IAdhocTicket;
import java.util.Date;

public class PaystationController 
		implements IPaystationController {
	
	private IPaystationUI ui;	
	private ICarpark carpark;

	private IAdhocTicket  adhocTicket = null;
	private float charge;
	
	

	public PaystationController(ICarpark carpark, IPaystationUI ui) {
		//TODO Implement constructor
                this.carpark = carpark;
                this.ui = ui;
                this.charge = -1;
                
                ui.registerController(this);
	}



	@Override
	public void ticketInserted(String barcode) {
		// TODO Auto-generated method stub
		adhocTicket = carpark.getAdhocTicket(barcode);
                if(adhocTicket == null){
                    ui.display("Rejected");
                    charge = -1;
                    return;
                }
                
                
                
                long entryDateTime = adhocTicket.getEntryDateTime();
                charge = carpark.calculateAddHocTicketCharge(entryDateTime);
                ui.display("$" + charge);
	}



	@Override
	public void ticketPaid() {
		// TODO Auto-generated method stub
		Date dte = new Date();
                long paidTime = dte.getTime();
                adhocTicket.pay(paidTime, charge);
                
                ui.printTicket(adhocTicket.getCarparkId(), adhocTicket.getTicketNo(), adhocTicket.getEntryDateTime(), adhocTicket.getPaidDateTime(), adhocTicket.getCharge(), adhocTicket.getBarcode());
                ui.display("Take Ticket");
	}



	@Override
	public void ticketTaken() {
		// TODO Auto-generated method stub
		adhocTicket = null;
                charge = -1;
                ui.display("");
	}

	
	
}
