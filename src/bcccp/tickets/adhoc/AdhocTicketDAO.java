package bcccp.tickets.adhoc;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class AdhocTicketDAO  implements IAdhocTicketDAO  {

	private IAdhocTicketFactory factory;
	private int currentTicketNo;
        private List<IAdhocTicket> adhocTickets;

	
	
	public AdhocTicketDAO(IAdhocTicketFactory factory) {
		//TODO Implement constructor
                this.factory = factory;
                
                currentTicketNo = 1;
                adhocTickets = new ArrayList<IAdhocTicket>();
	}



	@Override
	public IAdhocTicket createTicket(String carparkId) {
		// TODO Auto-generated method stub
		IAdhocTicket ticket = factory.make(carparkId, this.currentTicketNo);
                adhocTickets.add(ticket);
                currentTicketNo++;
                return ticket;
	}



	@Override
	public IAdhocTicket findTicketByBarcode(String barcode) {
		// TODO Auto-generated method stub                
                Iterator<IAdhocTicket> ticketIterator = adhocTickets.iterator();
                while(ticketIterator.hasNext()){
                    IAdhocTicket ticket = ticketIterator.next();
                    if(ticket.getBarcode().contentEquals(barcode)){
                        return ticket;
                    }
                }
		return null;
	}



	@Override
	public List<IAdhocTicket> getCurrentTickets() {
		// TODO Auto-generated method stub
		return adhocTickets;
	}	
}
