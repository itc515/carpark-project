package bcccp.tickets.adhoc;

import java.util.Date;

public class AdhocTicket implements IAdhocTicket {
	
	private String carparkId;
	private int ticketNo;
	private long entryDateTime;
	private long paidDateTime;
	private long exitDateTime;
	private float charge;
	private String barcode;

	
	
	public AdhocTicket(String carparkId, int ticketNo, String barcode) {
		//TDO Implement constructor
                this.carparkId = carparkId;
                this.ticketNo = ticketNo;
                this.barcode = barcode;
                
                entryDateTime = 0;
                paidDateTime = 0;
                exitDateTime = 0;
                charge = -1;
                
//                Date dte = new Date();
//                entryDateTime = dte.getTime();
	}


	@Override
	public int getTicketNo() {
		// TODO Auto-generated method stub
		return ticketNo;
	}


	@Override
	public String getBarcode() {
		// TODO Auto-generated method stub
		return barcode;
	}


	@Override
	public String getCarparkId() {
		// TODO Auto-generated method stub
		return carparkId;
	}


	@Override
	public void enter(long dateTime) {
		// TODO Auto-generated method stub
		this.entryDateTime = dateTime;
	}


	@Override
	public long getEntryDateTime() {
		// TODO Auto-generated method stub
		return entryDateTime;
	}


	@Override
	public boolean isCurrent() {
		// TODO Auto-generated method stub
                if(this.entryDateTime != 0.0 && this.exitDateTime == 0.0){
                    return true;
                }
                
		return false;
	}


	@Override
	public void pay(long dateTime, float charge) {
		// TODO Auto-generated method stub
		this.paidDateTime = dateTime;
                this.charge = charge;
	}


	@Override
	public long getPaidDateTime() {
		// TODO Auto-generated method stub
		return paidDateTime;
	}


	@Override
	public boolean isPaid() {
		// TODO Auto-generated method stub
                if(charge != -1 && paidDateTime != 0){
                    return true;
                }                
		return false;
	}


	@Override
	public float getCharge() {
		if(charge == -1){
                    return 0;
                }
		return charge;
	}


	@Override
	public void exit(long dateTime) {
		// TODO Auto-generated method stub
		this.exitDateTime = dateTime;
	}


	@Override
	public long getExitDateTime() {
		// TODO Auto-generated method stub
		return exitDateTime;
	}


	@Override
	public boolean hasExited() {
		// TODO Auto-generated method stub
                if(exitDateTime != 0){
                    return true;
                }
		return false;
	}	
}
