package bcccp.tickets.adhoc;

import java.util.Date;
import java.text.SimpleDateFormat;

public class AdhocTicketFactory implements IAdhocTicketFactory {

    @Override
    public IAdhocTicket make(String carparkId, int ticketNo) {
        SimpleDateFormat format = new SimpleDateFormat("ddmmyyyyHHmm");
        String barcode = "A" + ticketNo + format.format(new Date());
        return new AdhocTicket(carparkId, ticketNo, barcode);
    }
}
