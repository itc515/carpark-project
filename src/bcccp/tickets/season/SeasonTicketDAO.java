package bcccp.tickets.season;

import bcccp.tickets.season.ISeasonTicket;
import bcccp.tickets.season.IUsageRecordFactory;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class SeasonTicketDAO implements ISeasonTicketDAO {

	private IUsageRecordFactory factory;
        private List<ISeasonTicket> seasonTickets;

	
	
	public SeasonTicketDAO(IUsageRecordFactory factory) {
            this.factory = factory;
            
            seasonTickets = new ArrayList<ISeasonTicket>();
	}



	@Override
	public void registerTicket(ISeasonTicket ticket) {
		// TODO Auto-generated method stub
		if(!seasonTickets.contains(ticket)){
                    seasonTickets.add(ticket);
                }
	}



	@Override
	public void deregisterTicket(ISeasonTicket ticket) {
		// TODO Auto-generated method stub
                if(seasonTickets.contains(ticket)){
                    seasonTickets.remove(ticket);
                }		
	}



	@Override
	public int getNumberOfTickets() {
		// TODO Auto-generated method stub
		return seasonTickets.size();
	}



	@Override
	public ISeasonTicket findTicketById(String ticketId) {
		// TODO Auto-generated method stub
		Iterator<ISeasonTicket> ticketIterator = seasonTickets.iterator();
                while(ticketIterator.hasNext()){
                    ISeasonTicket ticket = ticketIterator.next();
                    if(ticket.getId().contentEquals(ticketId)){
                        return ticket;
                    }
                }
		return null;
	}



	@Override
	public void recordTicketEntry(String ticketId) {
		// TODO Auto-generated method stub
		ISeasonTicket ticket = findTicketById(ticketId);
                if(ticket != null){
                    Date dte = new Date();
                    long entryTime = dte.getTime();
                    IUsageRecord usage = factory.make(ticketId, entryTime);
                    ticket.recordUsage(usage);
                }
	}

	@Override
	public void recordTicketExit(String ticketId) {
		// TODO Auto-generated method stub
		ISeasonTicket ticket = findTicketById(ticketId);
                if(ticket != null){
                    Date dte = new Date();
                    long exitTime = dte.getTime();
                    ticket.endUsage(exitTime);
                }
	}	
}
